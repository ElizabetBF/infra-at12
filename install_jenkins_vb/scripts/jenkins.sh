echo "Install java......."
#install java
sudo apt-get update
sudo apt-get install -y openjdk-8-jdk

echo "Download Jenkins......."
#download and save jenkins
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -

echo "Add key Jenkins......."
#add deb http://pkg.jenkins.io/debian to /etc/apt/sources.list.d/jenkins.list
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'

echo "Install Jenkins......."
#jenkins
sudo apt-get update
sudo apt-get install -y jenkins

echo "Start Jenkins......."
#init jenkins
sudo systemctl enable jenkins
sudo systemctl restart jenkins
sudo systemctl status jenkins

echo "Enable firewall......."
#open firewall
sudo ufw allow 8080
sudo ufw enable
sudo ufw status

echo "Print password......."
#print password
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
